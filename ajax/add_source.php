<?php

include '../mysql/mysql.php';
include '../php/facebook.php';

session_start();

if (!array_key_exists('source_name', $_POST) || !array_key_exists('source_link', $_POST))
{
	echo json_encode(array(
		"status" => "fail",
		"error" => "0"
	));
	die();
}

$source_name = $_POST['source_name'];
$source_link = $_POST['source_link'];

if (!array_key_exists('fb_access_token', $_SESSION))
{
	echo json_encode(array(
		"status" => "fail",
		"error" => "1"
	));
	die();
}

$fb_access_token = $_SESSION['fb_access_token'];

$fb_user = GetFbUserArrayFromFbAccessToken($fb_access_token);

if (!array_key_exists('id', $fb_user))
{
	echo json_encode(array(
		"status" => "fail",
		"error" => "2"
	));
	die();
}

$fb_id = $fb_user['id'];

$uid = GetUIDFromFbID($fb_id, $mysql_db);

if ($uid == -1)
{
	echo json_encode(array(
		"status" => "fail",
		"error" => "3"
	));
	die();
}

$mysql_db->query("INSERT INTO ".MYSQL_PREFIX."source (name, link, uid) VALUES('$source_name', '$source_link', '$uid')");
if ($mysql_db->errno)
{
	echo json_encode(array(
		"status" => "fail",
		"error" => "4"
	));
	die();
}

$out_source_ids = array();
$out_source_names = array();
$out_source_link = array();

$result = $mysql_db->query("SELECT * FROM ".MYSQL_PREFIX."source WHERE uid='$uid' ORDER BY name");

$num_sources = $result->num_rows;

while ($row = $result->fetch_assoc())
{
	$out_source_ids[] = $row['id'];
	$out_source_names[] = $row['name'];
	$out_source_link[] = $row['link'];
}

$result->free_result();

$result = $mysql_db->query("SELECT * FROM ".MYSQL_PREFIX."source WHERE uid='$uid'");
$num_sources = $result->num_rows;
$result->free_result();

$result = $mysql_db->query("SELECT * FROM ".MYSQL_PREFIX."problems WHERE uid='$uid'");
$num_probs = $result->num_rows;
$result->free_result();

if ($num_sources)
	$average_probs_per_source = number_format($num_probs/$num_sources, 1);
else
	$average_probs_per_source = "N/A";

//Modify json encode code
echo json_encode(array(
	"status" => "pass",
	"num_sources" => $num_sources,
	"source_ids" => $out_source_ids,
	"source_names" => $out_source_names,
	"source_links" => $out_source_link,
	"num_probs" => $num_probs,
	"average_problem_per_source" => $average_probs_per_source,
));

?>