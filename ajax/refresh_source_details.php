<?php
include '../mysql/mysql.php';

//Submit status fail reasons:
//0 - POST parameter not set.
//1 - FB access token problem.
//2 - No such account.
//3 - MySQL error

session_start();

if (empty($_SESSION['fb_access_token']))
{
	echo json_encode(array("submit_status"=>"fail", "reason"=>"1"));
	die();
}

$fb_access_token = $_SESSION['fb_access_token'];

$fb_graph_url = "https://graph.facebook.com/me?access_token="
		.$fb_access_token;

$user = json_decode(file_get_contents($fb_graph_url));
if (empty($user->id))
{
	echo json_encode(array("submit_status"=>"fail", "reason"=>"1"));
	die();
}
$fb_id = $user->id;

$result = $mysql_db->query("SELECT * FROM ".MYSQL_PREFIX."users WHERE facebook_id='$fb_id'");
if (!$result->num_rows)
{
	$result->free_result();
	echo json_encode(array("submit_status"=>"fail", "reason"=>"2"));
	die();
}
$row = $result->fetch_assoc();
$result->free_result();
$uid = $row['uid'];

$result = $mysql_db->query("SELECT * FROM ".MYSQL_PREFIX."source WHERE uid='$uid'");
if ($mysql_db->errno)
{
	echo json_encode(array("submit_status"=>"fail", "reason"=>"3"));
	die();
}

$src_idx = array();
$src_name = array();
$src_link = array();

while ($row = $result->fetch_assoc())
{
	$src_idx[] = $row['id'];
	$src_name[] = $row['name'];
	$src_link[] = $row['link'];
}
$result->free_result();

echo json_encode(array("submit_status"=>"pass", "idx"=>$src_idx, "name"=>$src_name, "link"=>$src_link));
?>