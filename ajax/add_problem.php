<?php

include '../mysql/mysql.php';
include '../php/facebook.php';

session_start();

if (!array_key_exists('prob_num', $_POST) || !array_key_exists('prob_name', $_POST) || !array_key_exists('prob_source', $_POST) || !array_key_exists('prob_link', $_POST) || !array_key_exists('prob_tags', $_POST) || !array_key_exists('prob_date', $_POST))
{
	echo json_encode(array(
		"status" => "fail",
		"error" => "0"
	));
	die();
}

$prob_num = $_POST['prob_num'];
$prob_name = $_POST['prob_name'];
$prob_source = $_POST['prob_source'];
$prob_link = $_POST['prob_link'];
$prob_tags = $_POST['prob_tags'];
$prob_date = $_POST['prob_date'];

if (!array_key_exists('fb_access_token', $_SESSION))
{
	echo json_encode(array(
		"status" => "fail",
		"error" => "1"
	));
	die();
}

$fb_access_token = $_SESSION['fb_access_token'];

$fb_user = GetFbUserArrayFromFbAccessToken($fb_access_token);

if (!array_key_exists('id', $fb_user))
{
	echo json_encode(array(
		"status" => "fail",
		"error" => "2"
	));
	die();
}

$fb_id = $fb_user['id'];

$uid = GetUIDFromFbID($fb_id, $mysql_db);

if ($uid == -1)
{
	echo json_encode(array(
		"status" => "fail",
		"error" => "3"
	));
	die();
}

$mysql_db->query("INSERT INTO ".MYSQL_PREFIX."problems (prob_num, prob_name, source, link, tags, date, uid) VALUES('$prob_num', '$prob_name', '$prob_source', '$prob_link', '$prob_tags', '$prob_date', '$uid')");
if ($mysql_db->errno)
{
	echo json_encode(array(
		"status" => "fail",
		"error" => "4"
	));
	die();
}

$out_prob_ids = array();
$out_prob_nums = array();
$out_prob_names = array();
$out_prob_source = array();
$out_prob_link = array();
$out_prob_date = array();

$result = $mysql_db->query("SELECT * FROM ".MYSQL_PREFIX."problems WHERE uid='$uid' ORDER BY date DESC");

$num_problems = $result->num_rows;

while ($row = $result->fetch_assoc())
{
	$out_prob_ids[] = $row['id'];
	$out_prob_nums[] = $row['prob_num'];
	$out_prob_names[] = $row['prob_name'];
	$out_prob_source[] = $row['source'];
	$out_prob_link[] = $row['link'];
	$out_prob_date[] = $row['date'];
}

$result->free_result();

$result = $mysql_db->query("SELECT * FROM ".MYSQL_PREFIX."problems WHERE uid='$uid' ORDER BY date ASC LIMIT 1");
$row = $result->fetch_assoc();
$earliest_date = $row['date'];
$result->free_result();

$result = $mysql_db->query("SELECT * FROM ".MYSQL_PREFIX."problems WHERE uid='$uid' ORDER BY date DESC LIMIT 1");
$row = $result->fetch_assoc();
$latest_date = $row['date'];
$result->free_result();

$result = $mysql_db->query("SELECT DISTINCT date FROM ".MYSQL_PREFIX."problems WHERE uid='$uid'");
$num_active_days = $result->num_rows;
$result->free_result();

$result = $mysql_db->query("SELECT * FROM ".MYSQL_PREFIX."source WHERE uid='$uid'");
$num_sources = $result->num_rows;
$result->free_result();

$today = date("Y/m/d");
$start_time_stamp = strtotime($earliest_date);
$end_time_stamp = strtotime($today);
$total_days = (int)(abs($end_time_stamp-$start_time_stamp)/86400+1);

$start_time_stamp = strtotime($latest_date);
$inactive_days = (int)(abs($end_time_stamp-$start_time_stamp)/86400);

echo json_encode(array(
	"status" => "pass",
	"num_probs" => $num_problems,
	"prob_ids" => $out_prob_ids,
	"prob_nums" => $out_prob_nums,
	"prob_names" => $out_prob_names,
	"prob_sources" => $out_prob_source,
	"prob_links" => $out_prob_link,
	"prob_dates" => $out_prob_date,
	"num_active_days" => $num_active_days,
// 	"num_days" =>
));

?>