<?php
include '../mysql/mysql.php';

//Submit status fail reasons:
//0 - POST parameter not set.
//1 - FB access token problem.
//2 - No such account.
//3 - MySQL error.
//4 - Permission error.
//5 - No such item

session_start();

if (!isset($_POST['idx'], $_POST['prob_num'], $_POST['prob_name'], $_POST['source'], $_POST['link'], $_POST['tags'], $_POST['date']))
{
	echo json_encode(array("submit_status"=>"fail", "reason"=>"0"));
	die();
}

$id = $_POST['idx'];
$prob_num = $_POST['prob_num'];
$prob_name = $_POST['prob_name'];
$source = $_POST['source'];
$link = $_POST['link'];
$tags = $_POST['tags'];
$date = $_POST['date'];

if (empty($_SESSION['fb_access_token']))
{
	echo json_encode(array("submit_status"=>"fail", "reason"=>"1"));
	die();
}

$fb_access_token = $_SESSION['fb_access_token'];

$fb_graph_url = "https://graph.facebook.com/me?access_token="
		.$fb_access_token;

$user = json_decode(file_get_contents($fb_graph_url));
if (empty($user->id))
{
	echo json_encode(array("submit_status"=>"fail", "reason"=>"1"));
	die();
}
$fb_id = $user->id;

$result = $mysql_db->query("SELECT * FROM ".MYSQL_PREFIX."users WHERE facebook_id='$fb_id'");
if (!$result->num_rows)
{
	$result->free_result();
	echo json_encode(array("submit_status"=>"fail", "reason"=>"2"));
	die();
}
$row = $result->fetch_assoc();
$result->free_result();
$uid = $row['uid'];

$result = $mysql_db->query("SELECT * FROM ".MYSQL_PREFIX."problems WHERE id='$id'");
if (!$result->num_rows)
{
	echo json_encode(array("submit_status"=>"fail", "reason"=>"5"));
	$result->free_result();
	die();
}

$row = $result->fetch_assoc();
$result->free_result();
if ($row['uid'] != $uid)
{
	echo json_encode(array("submit_status"=>"fail", "reason"=>"4"));
	die();
}

$mysql_db->query("UPDATE ".MYSQL_PREFIX."problems SET prob_num='$prob_num', prob_name='$prob_name', source='$source', link='$link', tags='$tags', date='$date' WHERE id='$id'");
if ($mysql_db->errno)
{
	echo json_encode(array("submit_status"=>"fail", "reason"=>"3"));
	die();
}

$result=$mysql_db->query("SELECT * FROM ".MYSQL_PREFIX."problems WHERE id='$id'");
$output = $result->fetch_assoc();
$result->free_result();
$output['submit_status']="pass";
echo json_encode($output);
?>