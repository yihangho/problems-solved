<?php
include '../mysql/mysql.php';

session_start();

if (empty($_SESSION['fb_access_token']))
	die("Fatal error: Please refresh page.");

$fb_access_token = $_SESSION['fb_access_token'];

$fb_graph_url = "https://graph.facebook.com/me?access_token="
		.$fb_access_token;

$user = json_decode(file_get_contents($fb_graph_url));
if (empty($user->id))
	die("Fatal error: Please refresh page.");
$fb_id = $user->id;

$result = $mysql_db->query("SELECT * FROM ".MYSQL_PREFIX."users WHERE facebook_id='$fb_id'");
if (!$result->num_rows)
{
	$result->free_result();
	die("Fatal error: Please refresh page.");
}
$row = $result->fetch_assoc();
$result->free_result();
$uid = $row['uid'];

$result = $mysql_db->query("SELECT * FROM ".MYSQL_PREFIX."source WHERE uid='$uid' ORDER BY name");
?>

<?php if ($result->num_rows):?>
	<?php while ($row = $result->fetch_assoc()):?>
		<option value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>
	<?php endwhile;?>
<?php else:?>
	<option disabled selected>Add a new source first!</option>
<?php endif;?>

<?php $result->free_result();?>