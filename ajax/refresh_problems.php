<?php
include '../mysql/mysql.php';

session_start();

if (empty($_SESSION['fb_access_token']))
	die("Fatal error: Please refresh page.");

$fb_access_token = $_SESSION['fb_access_token'];

$fb_graph_url = "https://graph.facebook.com/me?access_token="
		.$fb_access_token;

$user = json_decode(file_get_contents($fb_graph_url));
if (empty($user->id))
	die("Fatal error: Please refresh page.");
$fb_id = $user->id;

$result = $mysql_db->query("SELECT * FROM ".MYSQL_PREFIX."users WHERE facebook_id='$fb_id'");
if (!$result->num_rows)
{
	$result->free_result();
	die("Fatal error: Please refresh page.");
}
$row = $result->fetch_assoc();
$result->free_result();
$uid = $row['uid'];

$result = $mysql_db->query("SELECT * FROM ".MYSQL_PREFIX."problems WHERE uid='$uid' ORDER BY date DESC");
?>

<?php if (!$result->num_rows):?>
	<?php $result->free_result();?>
	<div class="well well-small view-tab-item"><p style="margin: 0px;" class="text-info">There is nothing here. =)</p></div>
<?php else:?>
	<?php
		$tmp_result = $mysql_db->query("SELECT * FROM ".MYSQL_PREFIX."source");
		$dict = array();
		$link_dict = array();
		while ($tmp_row = $tmp_result->fetch_assoc())
		{
			$dict[$tmp_row['id']]=$tmp_row['name'];
			$link_dict[$tmp_row['id']]=$tmp_row['link'];
		}
		$tmp_result->free_result();
	?>
	<div style="display:none;" class="alert alert-error" id="delete-problem-fail">
		<strong>Oh snap.</strong> That problem cannot be deleted. Please try again.
	</div>
	<table class="table view-tab-item">
		<thead>
			<tr>
				<th>Problem number</th>
				<th>Problem name</th>
				<th>Source</th>
				<th>Date solved</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php while ($row = $result->fetch_assoc()):?>
				<tr id="problem-<?php echo $row['id'];?>">
					<td>
						<?php if (strlen($row['link'])>0):?>
							<a class="prob-a-<?php echo $row['id'];?>" href="<?php echo $row['link'];?>" target="_blank">
						<?php endif;?>
						<span id="prob-num-<?php echo $row['id'];?>"><?php echo $row['prob_num'];?></span>
						<?php if (strlen($row['link'])>0):?>
							</a>
						<?php endif;?>
					</td>
					<td>
						<?php if (strlen($row['link'])>0):?>
							<a class="prob-a-<?php echo $row['id'];?>" href="<?php echo $row['link'];?>" target="_blank">
						<?php endif;?>
						<span id="prob-name-<?php echo $row['id'];?>"><?php echo $row['prob_name'];?></span>
						<?php if (strlen($row['link'])>0):?>
							</a>
						<?php endif;?>
					</td>
					<td>
						<?php if (strlen($link_dict[$row['source']])>0):?>
							<a id="prob-source-a-<?php echo $row['id'];?>" href="<?php echo $link_dict[$row['source']];?>" target="_blank">
						<?php endif;?>
						<span id="prob-source-name-<?php echo $row['id'];?>"><?php echo $dict[$row['source']];?></span>
						<?php if (strlen($link_dict[$row['source']])>0):?>
							</a>
						<?php endif;?>
					</td>
					<td>
						<span id="prob-date-<?php echo $row['id'];?>"><?php echo $row['date'];?></span>
					</td>
					<td>
						<a href="#" onclick="return jqAjaxEditProblem(<?php echo $row['id'];?>)" title="Edit"><i class="icon-edit"></i></a> /
						<a href="#" onclick="return DeleteProblem(<?php echo $row['id'];?>)" title="Delete"><i class="icon-trash"></i></a>
					</td>
				</tr>
			<?php endwhile;?>
			<?php $result->free_result();?>
		</tbody>
	</table>
<?php endif;?>