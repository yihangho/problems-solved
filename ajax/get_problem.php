<?php
include '../mysql/mysql.php';

if (!isset($_POST['idx']))
{
	echo json_encode(array("submit_status"=>"fail"));
	die();
}

$id = $_POST['idx'];

$result = $mysql_db->query("SELECT * FROM ".MYSQL_PREFIX."problems where id='$id'");
if ($result->num_rows != 1)
{
	echo json_encode(array("submit_status"=>"fail"));
	$result->free_result();
	die();
}

$output = $result->fetch_assoc();
$output['submit_status']="pass";
$result->free_result();
echo json_encode($output);
?>