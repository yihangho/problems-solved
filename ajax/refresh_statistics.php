<?php
include '../mysql/mysql.php';

session_start();

if (empty($_SESSION['fb_access_token']))
	die("Fatal error: Please refresh page.");

$fb_access_token = $_SESSION['fb_access_token'];

$fb_graph_url = "https://graph.facebook.com/me?access_token="
		.$fb_access_token;

$user = json_decode(file_get_contents($fb_graph_url));
if (empty($user->id))
	die("Fatal error: Please refresh page.");
$fb_id = $user->id;

$result = $mysql_db->query("SELECT * FROM ".MYSQL_PREFIX."users WHERE facebook_id='$fb_id'");
if (!$result->num_rows)
{
	$result->free_result();
	die("Fatal error: Please refresh page.");
}
$row = $result->fetch_assoc();
$result->free_result();
$uid = $row['uid'];

$result = $mysql_db->query("SELECT * FROM ".MYSQL_PREFIX."problems WHERE uid='$uid' ORDER BY date ASC");
$num_probs_solved = $result->num_rows;
$row = $result->fetch_assoc();
$earliest_date = $row['date'];
$result->free_result();

$result = $mysql_db->query("SELECT * FROM ".MYSQL_PREFIX."problems WHERE uid='$uid' ORDER BY date DESC");
$row = $result->fetch_assoc();
$latest_date = $row['date'];
$result->free_result();

$result = $mysql_db->query("SELECT DISTINCT date FROM ".MYSQL_PREFIX."problems WHERE uid='$uid'");
$num_active_days = $result->num_rows;
$result->free_result();

$result = $mysql_db->query("SELECT * FROM ".MYSQL_PREFIX."source WHERE uid='$uid'");
$num_sources = $result->num_rows;
$result->free_result();

$today = date("Y/m/d");
$start_time_stamp = strtotime($earliest_date);
$end_time_stamp = strtotime($today);
$total_days = abs($end_time_stamp-$start_time_stamp)/86400+1;

$start_time_stamp = strtotime($latest_date);
$inactive_days = abs($end_time_stamp-$start_time_stamp)/86400;
?>
<div class="row-fluid">
	<div class="span6 statistics-left">Number of problems solved:</div>
	<div class="span6"><?php echo $num_probs_solved;?></div>
</div>
<div class="row-fluid">
	<div class="span6 statistics-left">Number of sources:</div>
	<div class="span6"><?php echo $num_sources;?></div>
</div>
<div class="row-fluid">
	<div class="span6 statistics-left">Number of active days:</div>
	<div class="span6"><?php echo $num_active_days;?></div>
</div>
<div class="row-fluid">
	<div class="span6 statistics-left">Number of days since the earliest problem:</div>
	<div class="span6"><?php echo $num_probs_solved?$total_days:"N/A";?></div>
</div>
<div class="row-fluid">
	<div class="span6 statistics-left">Percentage of active days:</div>
	<div class="span6"><?php echo $num_probs_solved?number_format($num_active_days/$total_days*100, 2)." %":"N/A";?></div>
</div>
<div class="row-fluid">
	<div class="span6 statistics-left">Average problems solved per active day:</div>
	<div class="span6"><?php echo $num_probs_solved?number_format($num_probs_solved / $num_active_days, 1):"N/A";?></div>
</div>
<div class="row-fluid">
	<div class="span6 statistics-left">Average problems solved per day:</div>
	<div class="span6"><?php echo $num_probs_solved?number_format($num_probs_solved / $total_days, 1):"N/A";?></div>
</div>
<div class="row-fluid">
	<div class="span6 statistics-left">Average problems solved per source:</div>
	<div class="span6"><?php echo $num_sources?number_format($num_probs_solved / $num_sources, 1):"N/A";?></div>
</div>
<div class="row-fluid">
	<div class="span6 statistics-left">Number of days since latest problem:</div>
	<div class="span6"><?php echo $num_probs_solved?$inactive_days:"N/A";?></div>
</div>