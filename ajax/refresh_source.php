<?php
include '../mysql/mysql.php';

session_start();

if (empty($_SESSION['fb_access_token']))
	die("Fatal error: Please refresh page.");

$fb_access_token = $_SESSION['fb_access_token'];

$fb_graph_url = "https://graph.facebook.com/me?access_token="
		.$fb_access_token;

$user = json_decode(file_get_contents($fb_graph_url));
if (empty($user->id))
	die("Fatal error: Please refresh page.");
$fb_id = $user->id;

$result = $mysql_db->query("SELECT * FROM ".MYSQL_PREFIX."users WHERE facebook_id='$fb_id'");
if (!$result->num_rows)
{
	$result->free_result();
	die("Fatal error: Please refresh page.");
}
$row = $result->fetch_assoc();
$result->free_result();
$uid = $row['uid'];

$result = $mysql_db->query("SELECT * FROM ".MYSQL_PREFIX."source WHERE uid='$uid' ORDER BY name");
?>

<?php if (!$result->num_rows):?>
	<div class="well well-small view-tab-item" style="max-width:100%;"><p style="margin: 0px;" class="text-info">There is nothing here. =)</p></div>
<?php else:?>
	<div class="alert alert-error" id="delete-source-fail-in-use">
		<strong>That source cannot be deleted!</strong> Some problems are using this source. If you really want to delete this source, delete those problems first.
	</div>
	<div class="alert alert-error" id="delete-source-fail">
		<strong>Operation failed!</strong> Please try again.
	</div>
	<table class="table view-tab-item" id="source_table">
		<thead>
			<tr>
				<th>Source</th>
				<th>Link</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
			<?php while ($row = $result->fetch_assoc()):?>
				<tr id="source-<?php echo $row['id'];?>">
					<td>
						<?php if (strlen($row['link'])>0):?>
							<a class="source-a-<?php echo $row['id'];?>" href="<?php echo $row['link'];?>" target="_blank">
						<?php endif;?>
						<span id="source-name-<?php echo $row['id'];?>"><?php echo $row['name'];?></span>
						<?php if (strlen($row['link'])>0):?>
							</a>
						<?php endif;?>
					</td>
					<td>
						<?php if (strlen($row['link'])>0):?>
							<a class="source-a-<?php echo $row['id'];?>" href="<?php echo $row['link'];?>" target="_blank">
						<?php endif;?>
						<span id="source-link-<?php echo $row['id'];?>"><?php echo $row['link'];?></span>
						<?php if (strlen($row['link'])>0):?>
							</a>
						<?php endif;?>
					</td>
					<td>
						<a href="#" onclick="return jqAjaxEditSource(<?php echo $row['id'];?>)" title="Edit"><i class="icon-edit"></i></a> /
						<a href="#" onclick="return DeleteSource(<?php echo $row['id'];?>)" title="Delete"><i class="icon-trash"></i></a>
					</td>
				</tr>
			<?php endwhile;?>
			<?php $result->free_result();?>
		</tbody>
	</table>
<?php endif;?>