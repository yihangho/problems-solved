<?php
include 'mysql.php';
$mysql_db->query("ALTER TABLE ".MYSQL_PREFIX."problems ADD uid INT");
if ($mysql_db->errno)
	die("MySQL Error: ".$mysql_db->errno);

$mysql_db->query("ALTER TABLE ".MYSQL_PREFIX."source ADD uid INT");
if ($mysql_db->errno)
	die("MySQL Error: ".$mysql_db->errno);

$mysql_db->query("CREATE TABLE ".MYSQL_PREFIX."users (uid INT NOT NULL AUTO_INCREMENT, PRIMARY KEY(uid), facebook_id VARCHAR(255))");
if ($mysql_db->errno)
	die("MySQL Error: ".$mysql_db->errno);
?>
<!doctype html>
<html>
<head>
	<title>Setup</title>
</head>
<body>
	Upgrade successful!
</body>
</html>