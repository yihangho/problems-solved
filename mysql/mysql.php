<?php
define("MYSQL_HOST", "localhost");
define("MYSQL_USER", "problems_solved");
define("MYSQL_PASSWORD", "h3po4H#PO$");
define("MYSQL_DATABASE", "problems_solved");
// define("MYSQL_PORT", "8888");
define("MYSQL_PREFIX", "");

if (defined("MYSQL_PORT"))
	$mysql_db = new mysqli(MYSQL_HOST, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE, MYSQL_PORT);
else
	$mysql_db = new mysqli(MYSQL_HOST, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE);

if ($mysql_db->connect_error)
	die("MySQL connection error: ".$mysql_db->connect_error);
?>