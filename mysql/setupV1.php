<?php
include 'mysql.php';
$mysql_db->query("CREATE TABLE ".MYSQL_PREFIX."problems (id INT NOT NULL AUTO_INCREMENT, PRIMARY KEY(id), source VARCHAR(255), prob_num VARCHAR(255), prob_name VARCHAR(255), link VARCHAR(255), tags VARCHAR(255), date date)");
if ($mysql_db->errno)
	die("MySQL Error: ".$mysql_db->errno);
$mysql_db->query("CREATE TABLE ".MYSQL_PREFIX."source (id INT NOT NULL AUTO_INCREMENT, PRIMARY KEY(id), name VARCHAR(255), link VARCHAR(255))");
if ($mysql_db->errno)
	die("MySQL Error: ".$mysql_db->errno);
?>
<!doctype html>
<html>
<head>
	<title>Setup</title>
</head>
<body>
	Setup successful!
</body>
</html>