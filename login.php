<?php
include "mysql/mysql.php";
include "php/facebook.php";

session_start();

if (array_key_exists('fb_access_token', $_SESSION))
{
	$uid = GetUIDFromFbAccessToken($_SESSION['fb_access_token'], $mysql_db);
	if ($uid != -1)
	{
		header("Location: index.php");
		die();
	}
}

$_SESSION['fb_state'] = md5(uniqid(rand(), TRUE));
$fb_dialog_url = "https://www.facebook.com/dialog/oauth?client_id="
	.FB_APP_ID."&redirect_uri=".urlencode(FB_REDIRECT)."&state="
	.$_SESSION['fb_state']."&scope=email";
?>

<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Problems Solved - Login</title>

		<!--Load CSS.-->
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/bootstrap-responsive.min.css">
		<link rel="stylesheet" href="css/login.css">
		<link rel="stylesheet" href="css/zocial.css">

		<!--Load JS. For those that have the option to use CDN, please still include a copy for development use and include the code to import them while have them commented.-->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<!--<script src="js/jquery-1.9.1.min.js"></script>-->
		<script src="js/bootstrap.min.js"></script>
		<script>
			$(document).ready(function(){
				$(".login-fb-button").hover(function(){
					$(this).css("background-color", "#8b9dc3");
				}, function(){
					$(this).css("background-color", "#3b5998");
				});
			});
		</script>
	</head>

	<body>
		<div class="row-fluid">
			<div class="span6 offset3 login-content">
				<h2>Please login</h2>
				<p>
					We need you to login to personalize the content and your experience.
				</p>
				<a href="<?php echo $fb_dialog_url;?>" class="zocial facebook" style="margin-bottom: 10px;">Login with Facebook</a>
			</div>
		</div>
	</body>
</html>