<?php
define("FB_APP_ID", "143382025832681");
define("FB_APP_SECRET", "ea09c205dd2ae5bd136870debaaa6c24");
define("FB_REDIRECT", "http://www.ps.loc/facebook_login.php");

function GetFbUserArrayFromFbAccessToken($access_token)
{
	$fb_graph_url = "https://graph.facebook.com/me?access_token="
		.$access_token;

	return json_decode(file_get_contents($fb_graph_url), true);
}

function GetFbIDFromFbAcessToken($access_token)
{
	$user = GetFbUserArrayFromFbAccessToken($access_token);

	if (!array_key_exists("id", $user))
		return -1;
	return $user['id'];
}

function GetUIDFromFbArray($user, $mysql_db)
{
	if (!array_key_exists("id", $user))
		return -1;

	$fb_id = $user['id'];

	$result = $mysql_db->query("SELECT * FROM ".MYSQL_PREFIX."users WHERE facebook_id = '$fb_id'");
	if ($result->num_rows)
	{
		$row = $result->fetch_assoc();
		$result->free_result();
		return $row['uid'];
	}
	else
	{
		$result->free_result();
		return -1;
	}
}

function GetUIDFromFbAccessToken($access_token, $mysql_db)
{
	$fb_id = GetFbIDFromFbAcessToken($access_token);

	if ($fb_id == -1) return -1;

	return GetUIDFromFbID($fb_id, $mysql_db);
}

function GetUIDFromFbID($fb_id, $mysql_db)
{
	$result = $mysql_db->query("SELECT * FROM ".MYSQL_PREFIX."users WHERE facebook_id = '$fb_id'");
	if ($result->num_rows)
	{
		$row = $result->fetch_assoc();
		$result->free_result();
		return $row['uid'];
	}
	else
	{
		$result->free_result();
		return -1;
	}
}
?>