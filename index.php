<?php
include "mysql/mysql.php";
include "php/facebook.php";

session_start();

if (!array_key_exists('fb_access_token', $_SESSION))
{
	header("Location: login.php");
	die();
}

$fb_access_token = $_SESSION['fb_access_token'];

$user = GetFbUserArrayFromFbAccessToken($fb_access_token);

if (!array_key_exists('id', $user))
{
	header("Location: login.php");
	die();
}

$fb_id = $user['id'];
$uid = GetUIDFromFbID($fb_id, $mysql_db);

if ($uid == -1)
{
	$mysql_db->query("INSERT INTO ".MYSQL_PREFIX."users (facebook_id) VALUES ('$fb_id')");
	$uid = $mysql_db->insert_id;
}
?>
<!doctype html>
<html>

<head>
	<meta charset="utf-8">
	<title>Problems Solved</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap-responsive.min.css">
	<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/zocial.css">
	<script src="js/jquery-1.9.1.min.js"></script>
	<script src="js/jquery-ui-1.10.2.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/script.js"></script>
	<script src="js/ajax.js"></script>
	<script>
		var source_dict = new Array();
		var source_link_dict = new Array();
		<?php $result = $mysql_db->query("SELECT * FROM ".MYSQL_PREFIX."source WHERE uid='$uid'"); ?>
		<?php while ($row = $result->fetch_assoc()):?>
			source_dict[<?php echo $row['id'];?>] = "<?php echo $row['name'];?>";
			source_link_dict[<?php echo $row['id'];?>] = "<?php echo $row['link'];?>";
		<?php endwhile;?>
	</script>
</head>

<body>
<div class="row-fluid">
<div class="span10 offset1">
<p style="margin-top:10px;">
	<img src="https://graph.facebook.com/<?php echo $fb_id;?>/picture">
	Hello <b><?php echo $user['first_name'];?></b>.
	<a href="facebook_logout.php" class="zocial facebook" style="float: right; margin-top: 10px;">Logout</a>
</p>
<div style="margin-top:20px;" class="tabbable">
	<ul class="nav nav-tabs">
		<li class="active"><a href="#tab1" data-toggle="tab"><i class="icon-plus"></i> Add</a></li>
		<li><a href="#tab2" data-toggle="tab"><i class="icon-book"></i> Sources</a></li>
		<li><a href="#tab3" data-toggle="tab"><i class="icon-search"></i> View</a></li>
		<li><a href="#tab4" data-toggle="tab"><i class="icon-user"></i> Me</a></li>
		<li><a href="#tab5" data-toggle="tab"><i class="icon-cogs"></i> Open Source</a></li>
		<li id="refresh" style="float:right;" onclick="jqAjaxRefreshAll()"><a href="index.php"><i class="icon-refresh"></i></a></li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="tab1">
			<div class="alert alert-success" id="add_problem_success_alert">
				<strong>Yay!</strong> Problem added!
			</div>
			<div class="alert alert-error" id="add_problem_fail_alert">
				<strong>Oh no!</strong> Problem cannot be added. Please try again.
			</div>
			<form class="form-horizontal" onsubmit="return jqAjaxAddProblem()">
				<div class="control-group">
					<label class="control-label" for="inputProbNum">Problem number</label>
					<div class="controls">
						<input type="text" name="inputProbNum" id="inputProbNum" placeholder="1A">
					</div>
  				</div>
  				<div class="control-group">
					<label class="control-label" for="inputProbName">Problem name</label>
					<div class="controls">
						<input type="text" name="inputProbName" id="inputProbName" placeholder="Theater Square">
					</div>
  				</div>
  				<div class="control-group">
					<label class="control-label" for="inputProbSource">Source</label>
					<div class="controls">
						<?php
							$result = $mysql_db->query("SELECT * FROM ".MYSQL_PREFIX."source WHERE uid='$uid' ORDER BY name");
						?>
						<select name="inputProbSource" id="inputProbSource">
							<?php if ($result->num_rows):?>
								<?php while ($row = $result->fetch_assoc()):?>
									<option value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>
								<?php endwhile;?>
							<?php else:?>
								<option disabled selected>Add a new source first!</option>
							<?php endif;?>
						</select>
						<?php $result->free_result();?>
					</div>
  				</div>
  				<div class="control-group">
					<label class="control-label" for="inputProbLink">Link</label>
					<div class="controls">
						<input type="url" name="inputProbLink" id="inputProbLink" placeholder="http://www.codeforces.com/problemset/problem/1/A">
					</div>
  				</div>
  				<div class="control-group">
					<label class="control-label" for="inputProbTags">Tags</label>
					<div class="controls">
						<input type="text" name="inputProbTags" id="inputProbTags" placeholder="greedy, math">
					</div>
  				</div>
				<div class="control-group">
					<label class="control-label" for="inputProbDate">Date</label>
					<div class="controls">
						<input type="date" name="inputProbDate" id="inputProbDate" required>
					</div>
  				</div>
				<div class="control-group">
					<div class="controls">
						<button type="submit" class="btn" id="add-problem-submit-btn">Add</button>
					</div>
				</div>
			</form>
		</div>
		<div class="tab-pane" id="tab2">
			<div class="alert alert-success" id="add_source_success_alert">
				<strong>Yay!</strong> Source added!
			</div>
			<div class="alert alert-error" id="add_source_fail_alert">
				<strong>Oh no!</strong> Source cannot be added. Please try again.
			</div>
			<form class="form-horizontal" onsubmit="return jqAjaxAddSource()">
				<div class="control-group">
					<label class="control-label" for="inputSourceName">Source name</label>
					<div class="controls">
						<input type="text" name="inputSourceName" id="inputSourceName" placeholder="Codeforces" required>
					</div>
  				</div>
  				<div class="control-group">
					<label class="control-label" for="inputSourceLink">Link</label>
					<div class="controls">
						<input type="url" name="inputSourceLink" id="inputSourceLink" placeholder="http://www.codeforces.com/">
					</div>
  				</div>
  				<div class="control-group">
					<div class="controls">
						<button type="submit" class="btn" id="add-source-submit-btn">Add</button>
					</div>
				</div>
			</form>
			<hr>

			<?php $result = $mysql_db->query("SELECT * FROM ".MYSQL_PREFIX."source WHERE uid='$uid' ORDER BY name");?>

			<div class="well well-small view-tab-item" id="no-source-label" style="max-width:100%;<?php if ($result->num_rows):?> display:none;<?php endif;?>">
				<p style="margin: 0px;" class="text-info">
					There is nothing here. =)
				</p>
			</div>

			<div class="alert alert-error" id="delete-source-fail-in-use">
				<strong>That source cannot be deleted!</strong> Some problems are using this source. If you really want to delete this source, delete those problems first.
			</div>

			<div class="alert alert-error" id="delete-source-fail">
				<strong>Operation failed!</strong> Please try again.
			</div>

			<table class="table view-tab-item" id="source-table" <?php if(!$result->num_rows):?>style="display:none;"<?php endif;?>>
				<thead>
					<tr>
						<th>Source</th>
						<th>Link</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php while ($row = $result->fetch_assoc()):?>
						<tr id="source-<?php echo $row['id'];?>">
							<td>
								<?php if (strlen($row['link'])>0):?>
									<a class="source-a-<?php echo $row['id'];?>" href="<?php echo $row['link'];?>" target="_blank">
								<?php endif;?>
								<span id="source-name-<?php echo $row['id'];?>"><?php echo $row['name'];?></span>
								<?php if (strlen($row['link'])>0):?>
									</a>
								<?php endif;?>
							</td>
							<td>
								<?php if (strlen($row['link'])>0):?>
									<a class="source-a-<?php echo $row['id'];?>" href="<?php echo $row['link'];?>" target="_blank">
								<?php endif;?>
								<span id="source-link-<?php echo $row['id'];?>"><?php echo $row['link'];?></span>
								<?php if (strlen($row['link'])>0):?>
									</a>
								<?php endif;?>
							</td>
							<td>
								<a href="#" onclick="return jqAjaxEditSource(<?php echo $row['id'];?>)" title="Edit"><i class="icon-edit"></i></a> /
								<a href="#" onclick="return DeleteSource(<?php echo $row['id'];?>)" title="Delete"><i class="icon-trash"></i></a>
							</td>
						</tr>
					<?php endwhile;?>
					<?php $result->free_result();?>
				</tbody>
			</table>
		</div>

		<div class="tab-pane" id="tab3">
			<?php $result = $mysql_db->query("SELECT * FROM ".MYSQL_PREFIX."problems WHERE uid='$uid' ORDER BY date DESC");?>

			<div class="well well-small view-tab-item" id="no-problem-label" <?php if ($result->num_rows):?>style="display:none;"<?php endif;?>>
				<p style="margin: 0px;" class="text-info">There is nothing here. =)</p>
			</div>

			<div class="alert alert-error" id="delete-problem-fail">
				<strong>Oh snap.</strong> That problem cannot be deleted. Please try again.
			</div>

			<?php
				$tmp_result = $mysql_db->query("SELECT * FROM ".MYSQL_PREFIX."source");
				$dict = array();
				$link_dict = array();
				while ($tmp_row = $tmp_result->fetch_assoc())
				{
					$dict[$tmp_row['id']]=$tmp_row['name'];
					$link_dict[$tmp_row['id']]=$tmp_row['link'];
				}
				$tmp_result->free_result();
			?>

			<table class="table view-tab-item" id="problems-table" <?php if (!$result->num_rows):?>style="display:none;"<?php endif;?>>
				<thead>
					<tr>
						<th>Problem number</th>
						<th>Problem name</th>
						<th>Source</th>
						<th>Date solved</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php while ($row = $result->fetch_assoc()):?>
					<tr id="problem-<?php echo $row['id'];?>">
						<td>
							<?php if (strlen($row['link'])>0):?>
								<a class="prob-a-<?php echo $row['id'];?>" href="<?php echo $row['link'];?>" target="_blank">
							<?php endif;?>
							<span id="prob-num-<?php echo $row['id'];?>"><?php echo $row['prob_num'];?></span>
							<?php if (strlen($row['link'])>0):?>
								</a>
							<?php endif;?>
						</td>
						<td>
							<?php if (strlen($row['link'])>0):?>
								<a class="prob-a-<?php echo $row['id'];?>" href="<?php echo $row['link'];?>" target="_blank">
							<?php endif;?>
							<span id="prob-name-<?php echo $row['id'];?>"><?php echo $row['prob_name'];?></span>
							<?php if (strlen($row['link'])>0):?>
								</a>
							<?php endif;?>
						</td>
						<td>
							<?php if (strlen($link_dict[$row['source']])>0):?>
								<a id="prob-source-a-<?php echo $row['id'];?>" href="<?php echo $link_dict[$row['source']];?>" target="_blank">
							<?php endif;?>
							<span id="prob-source-name-<?php echo $row['id'];?>"><?php echo $dict[$row['source']];?></span>
							<?php if (strlen($link_dict[$row['source']])>0):?>
								</a>
							<?php endif;?>
						</td>
						<td>
							<span id="prob-date-<?php echo $row['id'];?>"><?php echo $row['date'];?></span>
						</td>
						<td>
							<a href="#" onclick="return jqAjaxEditProblem(<?php echo $row['id'];?>)" title="Edit"><i class="icon-edit"></i></a> /
							<a href="#" onclick="return DeleteProblem(<?php echo $row['id'];?>)" title="Delete"><i class="icon-trash"></i></a>
						</td>
					</tr>
					<?php endwhile;?>
					<?php $result->free_result();?>
				</tbody>
			</table>
		</div>
		<?php
			$result = $mysql_db->query("SELECT * FROM ".MYSQL_PREFIX."problems WHERE uid='$uid' ORDER BY date ASC");
			$num_probs_solved = $result->num_rows;
			$row = $result->fetch_assoc();
			$earliest_date = $row['date'];
			$result->free_result();

			$result = $mysql_db->query("SELECT * FROM ".MYSQL_PREFIX."problems WHERE uid='$uid' ORDER BY date DESC");
			$row = $result->fetch_assoc();
			$latest_date = $row['date'];
			$result->free_result();

			$result = $mysql_db->query("SELECT DISTINCT date FROM ".MYSQL_PREFIX."problems WHERE uid='$uid'");
			$num_active_days = $result->num_rows;
			$result->free_result();

			$result = $mysql_db->query("SELECT * FROM ".MYSQL_PREFIX."source WHERE uid='$uid'");
			$num_sources = $result->num_rows;
			$result->free_result();

			$today = date("Y/m/d");
			$start_time_stamp = strtotime($earliest_date);
			$end_time_stamp = strtotime($today);
			$total_days = (int)(abs($end_time_stamp-$start_time_stamp)/86400+1);

			$start_time_stamp = strtotime($latest_date);
			$inactive_days = (int)(abs($end_time_stamp-$start_time_stamp)/86400);
		?>
		<div class="tab-pane" id="tab4">
			<div class="row-fluid">
				<div class="span6 statistics-left">Number of problems solved:</div>
				<div class="span6">
					<span id="stats-num-prob-solved"><?php echo $num_probs_solved;?></span>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span6 statistics-left">Number of sources:</div>
				<div class="span6">
					<span id="stats-num-source"><?php echo $num_sources;?></span>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span6 statistics-left">Number of active days:</div>
				<div class="span6">
					<span id="stats-num-active-days"><?php echo $num_active_days;?></span>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span6 statistics-left">Number of days since the earliest problem:</div>
				<div class="span6">
					<span id="stats-num-days"><?php echo $num_probs_solved?$total_days:"N/A";?></span>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span6 statistics-left">Percentage of active days:</div>
				<div class="span6">
					<span id="stats-percentage-active"><?php echo $num_probs_solved?number_format($num_active_days/$total_days*100, 2)." %":"N/A";?></span>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span6 statistics-left">Average problems solved per active day:</div>
				<div class="span6">
					<span id="stats-ave-prob-solved-per-active-day"><?php echo $num_probs_solved?number_format($num_probs_solved / $num_active_days, 1):"N/A";?></span>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span6 statistics-left">Average problems solved per day:</div>
				<div class="span6">
					<span id="stats-ave-prob-solved-per-day"><?php echo $num_probs_solved?number_format($num_probs_solved / $total_days, 1):"N/A";?></span>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span6 statistics-left">Average problems solved per source:</div>
				<div class="span6">
					<span id="stats-ave-prob-solved-per-source"><?php echo $num_sources?number_format($num_probs_solved / $num_sources, 1):"N/A";?></span>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span6 statistics-left">Number of days since latest problem:</div>
				<div class="span6">
					<span id="stats-num-days-since-last-prob"><?php echo $num_probs_solved?$inactive_days:"N/A";?></span>
				</div>
			</div>
		</div>
		<div class="tab-pane" id="tab5">
			<a href="https://bitbucket.org/yihangho/problems-solved/overview" target="_blank">Visit project page</a>
			<hr>
			This application is powered by other Open Source projects. One of the best ways to support them is to link back. Here is the list of Open Source projects that are used by this application:
			<ol>
				<li> <a href="http://jquery.com/" target="_blank">jQuery</a> </li>
				<li> <a href="http://jqueryui.com/" target="_blank">jQuery UI</a> </li>
				<li> <a href="http://twitter.github.com/bootstrap/index.html" target="_blank">Twitter Bootstrap</a> </li>
				<li> <a href="http://fortawesome.github.com/Font-Awesome/" target="_blank">Font Awesome</a> </li>
				<li> <a href="https://github.com/samcollins/css-social-buttons" target="_blank">Zocial CSS Social Buttons</a> </li>
			</ol>
		</div>
	</div>
</div>
</div>
<div id="edit-source-modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="edit-source-label" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i></button>
		<h3 id="edit-source-label">Edit Source</h3>
	</div>
	<form id="edit-source-form" class="form-horizontal" onsubmit="return jqAjaxSaveSource()">
	<div class="modal-body">
		<div class="control-group">
			<label class="control-label" for="editSourceName">Source name</label>
			<div class="controls">
				<input type="text" name="editSourceName" id="editSourceName" placeholder="Codeforces" required>
			</div>
  		</div>
  		<div class="control-group">
			<label class="control-label" for="editSourceLink">Link</label>
			<div class="controls">
				<input type="url" name="editSourceLink" id="editSourceLink" placeholder="http://www.codeforces.com/">
			</div>
  		</div>
	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
		<button type="submit" class="btn btn-primary" id="save-source-submit-btn">Save <i class="icon-save"></i></button>
	</div>
	</form>
</div>
<div id="delete-source-confirmation-modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="delete-source-label" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i></button>
		<h3 id="delete-source-label">Delete source</h3>
	</div>
	<div class="modal-body">
		Are you sure you want to delete '<a id="delete-source-confirmation-source-link" href="#"><span id="delete-source-confirmation-source-name">source</span></a>'?
	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">No</button>
		<button id="confirm-delete-source" class="btn btn-primary">Delete</button>
	</div>
</div>

<div id="delete-problem-modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="delete-problem-label" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i></button>
		<h3 id="delete-problem-label">Delete Problem</h3>
	</div>
	<div class="modal-body">
		Are you sure you want to delete '<a id="delete-problem-link" href=""><span id="delete-problem-name"></span></a>'?
	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">No</button>
		<button id="confirm-delete-problem" class="btn btn-primary">Delete <i class="icon-trash"></i></button>
	</div>
</div>
</div>

<div id="edit-problem-modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="edit-prob-label" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="icon-remove"></i></button>
		<h3 id="edit-prob-label">Edit Problem</h3>
	</div>
	<form id="edit-problem-form" class="form-horizontal" onsubmit="return jqAjaxSaveProblem()">
	<div class="modal-body">
		<div class="control-group">
			<label class="control-label" for="editProbNum">Problem number</label>
			<div class="controls">
				<input type="text" name="editProbNum" id="editProbNum" placeholder="1A">
			</div>
  		</div>
  		<div class="control-group">
			<label class="control-label" for="editProbName">Problem name</label>
			<div class="controls">
				<input type="text" name="editProbName" id="editProbName" placeholder="Theater Square">
			</div>
  		</div>
  		<div class="control-group">
			<label class="control-label" for="editProbSource">Source</label>
			<div class="controls">
				<?php
					$result = $mysql_db->query("SELECT * FROM ".MYSQL_PREFIX."source WHERE uid='$uid' ORDER BY name");
				?>
				<select name="editProbSource" id="editProbSource">
					<?php while ($row = $result->fetch_assoc()):?>
						<option value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>
					<?php endwhile;?>
				</select>
				<?php $result->free_result();?>
			</div>
  		</div>
  		<div class="control-group">
			<label class="control-label" for="editProbLink">Link</label>
			<div class="controls">
				<input type="url" name="editProbLink" id="editProbLink" placeholder="http://www.codeforces.com/problemset/problem/1/A">
			</div>
  		</div>
  		<div class="control-group">
			<label class="control-label" for="editProbTags">Tags</label>
			<div class="controls">
				<input type="text" name="editProbTags" id="editProbTags" placeholder="greedy, math">
			</div>
  		</div>
		<div class="control-group">
			<label class="control-label" for="inputProbDate">Date</label>
			<div class="controls">
				<input type="date" name="editProbDate" id="editProbDate" required>
			</div>
  		</div>
	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
		<button id="save-problems-submit-btn" type="submit" class="btn btn-primary">Save <i class="icon-save"></i></button>
	</div>
	</form>
</div>
</body>
</html>