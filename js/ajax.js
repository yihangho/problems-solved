// This file contains all JavaScript code that are related to Ajax
// Ajax functions related to Problems
function jqAjaxAddProblem()
{
	if ($("#add-problem-submit-btn").hasClass("disabled"))
		return false;

	$("#add-problem-submit-btn").addClass("disabled");
	$("#add-problem-submit-btn").html('Adding <i class="icon-spinner icon-spin"></i>');

	$.ajax({
		type: "POST",
		url: "ajax/add_problem.php",
		data: {
			prob_num: $("#inputProbNum").val(),
			prob_name: $("#inputProbName").val(),
			prob_source: $("#inputProbSource").val(),
			prob_link: $("#inputProbLink").val(),
			prob_tags: $("#inputProbTags").val(),
			prob_date: $("#inputProbDate").val()
		},
		success: function(data){
			$("#add-problem-submit-btn").removeClass("disabled");
 			$("#add-problem-submit-btn").html('Add');

			if (data.status == "pass")
			{
				$("#add_problem_success_alert").fadeIn();

				RefreshProblemsTable(data.num_probs, data.prob_ids, data.prob_nums, data.prob_names, data.prob_links, data.prob_sources, data.prob_tags, data.prob_dates);

				$("#stats-num-prob-solved").text(data.num_probs);
				$("#stats-num-active-days").text(data.num_active_days);
				$("#stats-num-days").text(data.num_days);
				$("#stats-percentage-active").text(data.percentage_active);
				$("#stats-ave-prob-solved-per-active-day").text(data.ave_prob_solved_per_active_day);
				$("#stats-ave-prob-solved-per-day").text(data.ave_prob_solved_per_day);
				$("#stats-ave-prob-solved-per-source").text(data.ave_prob_solved_per_source);
				$("#stats-num-days-since-last-prob").text(data.num_days_since_last_prob);
				ClearAddProblemForm();
			}
			else if (data.error == "1" || data.error == "2" || data.error == "3")
				window.location.href = "login.php";
			else
				$("#add_problem_fail_alert").fadeIn();
		},
		beforeSend: function(){
			HideMessages();
		},
		error: function(){
			$("#add-problem-submit-btn").removeClass("disabled");
 			$("#add-problem-submit-btn").html('Add');
 			$("#add_problem_fail_alert").fadeIn();
		},
		dataType: "json"
	});

	return false;
}

function jqAjaxEditProblem(idx)
{
	$.post("ajax/get_problem.php",
		{idx: idx},
		function(data){
			if (data.submit_status == "pass")
			{
				$("#edit-problem-form").attr("onsubmit", "return jqAjaxSaveProblem("+idx+")");
				$("#editProbNum").attr("placeholder", data.prob_num);
				$("#editProbNum").val(data.prob_num);
				$("#editProbName").attr("placeholder", data.prob_name);
				$("#editProbName").val(data.prob_name);
				$("#editProbSource").val(data.source);
				$("#editProbLink").val(data.link);
				$("#editProbLink").attr("placeholder", data.link);
				$("#editProbTags").val(data.tags);
				$("#editProbTags").attr("placeholder", data.tags);
				$("#editProbDate").val(data.date);
				$("#edit-problem-modal").modal('show');
			}
		}, "json");
	return false;
}

function jqAjaxSaveProblem(idx)
{
	if ($("#save-problems-submit-btn").hasClass("disabled"))
		return false;

	$("#save-problems-submit-btn").addClass("disabled");
	$("#save-problems-submit-btn").html('Saving <i class="icon-spinner icon-spin"></i>');

	$.post("ajax/save_problem.php",
		{idx: idx,
		prob_num: $("#editProbNum").val(),
		prob_name: $("#editProbName").val(),
		source: $("#editProbSource").val(),
		link: $("#editProbLink").val(),
		tags: $("#editProbTags").val(),
		date: $("#editProbDate").val()},
		function(data){
			$("#edit-problem-modal").modal('hide');
			$("#save-problems-submit-btn").removeClass("disabled");
			$("#save-problems-submit-btn").html('Save <i class="icon-save"></i>');
			if (data.submit_status == "pass")
			{
				$("#prob-num-"+idx).text(data.prob_num);
				$("#prob-name-"+idx).text(data.prob_name);
				$("#prob-source-name-"+idx).text(source_dict[data.source]);
				$("#prob-date-"+idx).text(data.date);
				if ($(".prob-a-"+idx).length != 0)
				{
					$("#prob-num-"+idx).unwrap();
					$("#prob-name-"+idx).unwrap();
				}
				if (data.link.length != 0)
				{
					$("#prob-num-"+idx).wrap('<a class="prob-a-'+idx+'" href="'+data.link+'">');
					$("#prob-name-"+idx).wrap('<a class="prob-a-'+idx+'" href="'+data.link+'">');
				}
				if ($("#prob-source-a-"+idx).length != 0)
					$("#prob-source-name-"+idx).unwrap();
				if (source_link_dict[data.source].length != 0)
					$("#prob-source-name-"+idx).wrap('<a id="prob-source-a-'+idx+'" href="'+source_link_dict[data.source]+'">');
			}
			else if (data.reason == "1" || data.reason == "2" || data.reason == "4" || data.reason == "5")
				window.location.href = "index.php";
		}, "json");
	return false;
}

function jqAjaxDeleteProblem(idx)
{
	if ($("#confirm-delete-problem").hasClass("disabled"))
		return false;

	$("#confirm-delete-problem").addClass("disabled");
	$("#confirm-delete-problem").html('Deleting <i class="icon-spinner icon-spin"></i>');


	$.post("ajax/delete_problem.php",
		{idx: idx},
		function(data){
			HideMessages();
			$("#delete-problem-modal").modal('hide');
			$("#confirm-delete-problem").removeClass("disabled");
			$("#confirm-delete-problem").html('Delete <i class="icon-trash"></i>');
			if (data.submit_status == "pass")
				$("#problem-"+idx).remove();
			else if (data.reason == "1" || data.reason == "2" || data.reason == "4" || data.reason == "5")
				window.location.href = "index.php";
			else
			{
				$("#delete-problem-fail").fadeIn();
				Flash("#problem-"+idx, "#f2dede", "#fff");
			}
			jqAjaxRefreshStatistics();
		}, "json");
}

// Ajax functions related to Sources

function jqAjaxAddSource()
{
	if ($("#add-source-submit-btn").hasClass("disabled"))
		return false;

	$("#add-source-submit-btn").addClass("disabled");
	$("#add-source-submit-btn").html('Adding <i class="icon-spinner icon-spin"></i>');

	$.ajax({
		type: "POST",
		url: "ajax/add_source.php",
		data: {
			source_name: $("#inputSourceName").val(),
			source_link: $("#inputSourceLink").val()
		},
		success: function(data){
			$("#add-source-submit-btn").removeClass("disabled");
 			$("#add-source-submit-btn").html('Add');

			if (data.status == "pass")
			{
				$("#add_source_success_alert").fadeIn();

				RefreshSourceTable(data.num_sources, data.source_ids, data.source_names, data.source_links);

				$("#stats-num-source").text(data.num_sources);
				$("#stats-ave-prob-solved-per-source").text(data.average_problem_per_source);

				source_dict.length = 0;
				source_link_dict.length = 0;
				for (var i = 0; i<data.num_sources; i++)
				{
					source_dict[data.source_ids[i]] = data.source_names[i];
					source_link_dict[data.source_ids[i]] = data.source_links[i];
				}

				ClearAddSourceForm();
			}
			else if (data.error == "1" || data.error == "2" || data.error == "3")
				window.location.href = "login.php";
			else
				$("#add_source_fail_alert").fadeIn();
		},
		beforeSend: function(){
			HideMessages();
		},
		error: function(){
			$("#add-source-submit-btn").removeClass("disabled");
 			$("#add-source-submit-btn").html('Add');
 			$("#add_source_fail_alert").fadeIn();
		},
		dataType: "json"
	});

	return false;
}

function jqAjaxEditSource(idx)
{
	$.post("ajax/get_source.php",
		{idx: idx},
		function(data){
			if (data.submit_status == "pass")
			{
				$("#editSourceName").val(data.name);
				$("#editSourceName").attr("placeholder", data.name);
				$("#editSourceLink").val(data.link);
				$("#editSourceLink").attr("placeholder", data.link);
				$("#edit-source-form").attr("onsubmit", "return jqAjaxSaveSource("+idx+")");
				$("#edit-source-modal").modal('show');
			}
		}, "json");
}

function jqAjaxSaveSource(idx)
{
	if ($("#save-source-submit-btn").hasClass("disabled"))
		return false;

	$("#save-source-submit-btn").addClass("disabled");
	$("#save-source-submit-btn").html('Saving <i class="icon-spinner icon-spin"></i>');

	$.post("ajax/save_source.php",
		{idx: idx,
		name: $("#editSourceName").val(),
		link: $("#editSourceLink").val()},
		function(data){
			$("#edit-source-modal").modal('hide');
			$("#save-source-submit-btn").removeClass("disabled");
			$("#save-source-submit-btn").html('Save <i class="icon-save"></i>');
			if (data.submit_status == "pass")
			{
				Flash("#source-"+idx, "#dff0d8", "#fff");
				if ($(".source-a-"+idx).length != 0)
				{
					$("#source-link-"+idx).unwrap();
					$("#source-name-"+idx).unwrap();
				}
				if (data.link.length != 0)
				{
					$("#source-link-"+idx).wrap('<a class="source-a-'+idx+'" href="'+data.link+'">');
					$("#source-name-"+idx).wrap('<a class="source-a-'+idx+'" href="'+data.link+'">');
				}
				$("#source-link-"+idx).text(data.link);
				$("#source-name-"+idx).text(data.name);
				$("option[value="+idx+"]").text(data.name);
				jqAjaxRefreshProblems();
			}
			else if (data.reason == "1" || data.reason == "2" || data.reason == "4" || data.reason == "5")
			{
				alert("Prob..");
				window.location.href = "index.php";
			}
			else
				Flash("#source-"+idx, "#f2dede", "#fff");
		}, "json");
	return false;
}

function jqAjaxDeleteSource(idx)
{
	if ($("#confirm-delete-source").hasClass("disabled"))
		return false;

	$("#confirm-delete-source").addClass("disabled");
	$("#confirm-delete-source").html('Deleting <i class="icon-spin icon-spinner"></i>');

	$.post("ajax/delete_source.php",
		{idx: idx},
		function(data){
			HideMessages();
			$("#delete-source-confirmation-modal").modal('hide');
			$("#confirm-delete-source").removeClass("disabled");
			$("#confirm-delete-source").html('Delete <i class="icon-trash"></i>');
			if (data.submit_status == "pass")
			{
				$("#source-"+idx).remove();
				if ($("#source-table tbody tr").length == 0)
					jqAjaxRefreshSource();
			}
			else if (data.reason == "6")
				$("#delete-source-fail-in-use").fadeIn();
			else
			{
				alert(data.reason);
				$("#delete-source-fail").fadeIn();
			}
			jqAjaxRefreshStatistics();
			jqAjaxRefreshSourceOption();
			jqAjaxRefreshSourceDetails();
		}, "json");
	return false;
}

// Ajax functions related to refreshing

function jqAjaxRefreshSource()
{
	$("#tab2 hr").nextAll().hide();
	$("#tab2").append('<div id="source_table_spinner" class="span2 offset5"><i class="icon-spinner icon-spin icon-4x"></i></div>');
	$.post("ajax/refresh_source.php", {},
		function(data){
			$("#tab2 hr").nextAll().remove();
			$("#source_table_spinner").remove();
			$("#tab2").append(data);
		});
}

function jqAjaxRefreshSourceOption()
{
	$("#inputProbSource").hide();
	$("#editProbSource").hide();
	$('<div class="source_option_spinner" style="margin-top:6px;"><i class="icon-spinner icon-spin"></i></div>').insertAfter("#inputProbSource");
	$('<div class="source_option_spinner" style="margin-top:6px;"><i class="icon-spinner icon-spin"></i></div>').insertAfter("#editProbSource");
	$.post("ajax/refresh_source_option.php", {},
		function(data){
			$("#inputProbSource").empty();
			$("#editProbSource").empty();
			$("#inputProbSource").append(data);
			$("#editProbSource").append(data);
			$(".source_option_spinner").remove();
			$("#inputProbSource").show();
			$("#editProbSource").show();
		});
}

function jqAjaxRefreshProblems()
{
	$("#tab3").children().hide();
	$("#tab3").append('<div id="problems_table_spinner" class="span2 offset5"><i class="icon-spinner icon-spin icon-4x"></i></div>');
	$.post("ajax/refresh_problems.php", {},
		function(data){
			$("#tab3").children().remove();
			$("#problems_table_spinner").remove();
			$("#tab3").append(data);
		});
}

function jqAjaxRefreshSourceDetails()
{
	$.post("ajax/refresh_source_details.php", {},
		function(data){
			if (data.submit_status == "pass")
			{
				source_dict.length = 0;
				source_link_dict.length = 0;
				for (var i = 0; i<data.idx.length; i++)
				{
					source_dict[data.idx[i]] = data.name[i];
					source_link_dict[data.idx[i]] = data.link[i];
				}
			}
			else if (data.reason == "1" || data.reason == "2")
				window.location.href = "index.php";
		}, "json");
}

function jqAjaxRefreshStatistics()
{
	$("#tab4").children().hide();
	$("#tab4").append('<div id="statistics_spinner" class="span2 offset5"><i class="icon-spinner icon-spin icon-4x"></i></div>');
	$.post("ajax/refresh_statistics.php", {},
		function(data){
			$("#tab4").empty();
			$("#tab4").append(data);
		});
}

//New Refresh functions - Save bandwidth (Do only necessary refreshes)
function jqAjaxPostAddSourceRefresh()
{
	$.ajax({
		type: "POST",
		url: "ajax/post_add_source_refresh.php",
		success: function(data){
			alert(data);
		},
		beforeSend: function(){
			alert("Before sends");
		},
		error: function(){
			alert("Refresh fails");
		},
	});
}