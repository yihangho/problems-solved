// This file contains all JavaScript code that are not related to Ajax

function HideMessages()
{
	$(".alert").hide();
}

function ClearAddProblemForm()
{
	$("#inputProbNum").val("");
	$("#inputProbName").val("");
	$("#inputProbSource").val($("#inputSource option").first().val());
	$("#inputProbLink").val("");
	$("#inputProbTags").val("");
	$("#inputProbDate").val("");
}

function ClearAddSourceForm()
{
	$("#inputSourceName").val("");
	$("#inputSourceLink").val("");
}

function Flash(selector, color1, color2)
{
	$(selector).animate(
	{
		backgroundColor: color1
	}, 100, function(){
		$(selector).animate(
		{
			backgroundColor: color2
		}, 750);
	});
}

function DeleteSource(idx)
{
	if ($("#delete-source-confirmation-source-link").length != 0)
		$("#delete-source-confirmation-source-name").unwrap();
	if ($(".source-a-"+idx).length != 0)
		$("#delete-source-confirmation-source-name").wrap('<a id="delete-source-confirmation-source-link" href="'+$(".source-a-"+idx).attr("href")+'">');
	$("#delete-source-confirmation-source-name").text($("#source-name-"+idx).text());
	$("#confirm-delete-source").attr("onclick", "jqAjaxDeleteSource("+idx+")");
	$("#delete-source-confirmation-modal").modal('show');
}

function DeleteProblem(idx)
{
	if ($("#delete-problem-link").length != 0)
		$("#delete-problem-name").unwrap();
	if ($(".prob-a-"+idx).length != 0)
		$("#delete-problem-name").wrap('<a id="delete-problem-link" href="'+$(".prob-a-"+idx).attr("href")+'">');
	$("#delete-problem-name").text($("#prob-name-"+idx).text());
	$("#confirm-delete-problem").attr("onclick", "jqAjaxDeleteProblem("+idx+")");
	$("#delete-problem-modal").modal('show');
}

function RefreshProblemsTable(num_probs, prob_ids, prob_nums, prob_names, prob_links, prob_sources, prob_tags, prob_dates)
{
	if (num_probs == 0)
	{
		$("#no-problem-label").fadeIn();
		$("#problems-table").fadeOut();
	}
	else
	{
		$("#no-problem-label").fadeOut();
		$("#problems-table").fadeIn();
		$("#problems-table tbody").empty();
		for (var i =0; i < num_probs; i++)
		{
			$("#problems-table tbody").append('<tr id="problem-'+prob_ids[i]+'"></tr>');
			$("#problem-"+prob_ids[i]).append('<td><span id="prob-num-'+prob_ids[i]+'">'+prob_nums[i]+'</span></td>');
			$("#problem-"+prob_ids[i]).append('<td><span id="prob-name-'+prob_ids[i]+'">'+prob_names[i]+'</span></td>');
			$("#problem-"+prob_ids[i]).append('<td><span id="prob-source-name-'+prob_ids[i]+'">'+source_dict[prob_sources[i]]+'</span></td>');
			$("#problem-"+prob_ids[i]).append('<td><span id="prob-date-'+prob_ids[i]+'">'+prob_dates[i]+'</span></td>');
			$("#problem-"+prob_ids[i]).append('<td><a href="#" onclick="return jqAjaxEditProblem('+prob_ids[i]+')" title="Edit"><i class="icon-edit"></i></a> / <a href="#" onclick="return DeleteProblem('+prob_ids[i]+')" title="Delete"><i class="icon-trash"></i></a>');

			if (prob_links[i].length != 0)
			{
				$("#prob-num-"+prob_ids[i]).wrap('<a class="prob-a-'+prob_ids[i]+'" href="'+prob_links[i]+'" target="_blank">');
				$("#prob-name-"+prob_ids[i]).wrap('<a class="prob-a-'+prob_ids[i]+'" href="'+prob_links[i]+'" target="_blank">');
			}

			if (source_link_dict[prob_sources[i]].length != 0)
				$("#prob-source-name-"+prob_ids[i]).wrap('<a id="prob-source-a-'+prob_ids[i]+'" href="'+source_link_dict[prob_sources[i]]+'" target="_blank">');
		}
	}
}

function RefreshSourceTable(num_sources, source_ids, source_names, source_links)
{
	if (num_sources == 0)
	{
		$("#no-source-label").fadeIn();
		$("#source-table").fadeOut();
	}
	else
	{
		$("#no-source-label").fadeOut();
		$("#source-table").fadeIn();
		$("#source-table tbody").empty();
		$("#inputProbSource").empty();
		$("#editProbSource").empty();

		for (var i =0; i < num_sources; i++)
		{
			$("#source-table tbody").append('<tr id="source-'+source_ids[i]+'"></tr>');
			$("#source-"+source_ids[i]).append('<td><span id="source-name-'+source_ids[i]+'">'+source_names[i]+'</span></td>');
			$("#source-"+source_ids[i]).append('<td><span id="source-link-'+source_ids[i]+'">'+source_links[i]+'</span></td>');
			$("#source-"+source_ids[i]).append('<td><a href="#" onclick="return jqAjaxEditSource('+source_ids[i]+')" title="Edit"><i class="icon-edit"></i></a> / <a href="#" onclick="return DeleteSource('+source_ids[i]+')" title="Delete"><i class="icon-trash"></i></a>');

			if (source_links[i].length != 0)
			{
				$("#source-name-"+source_ids[i]).wrap('<a class="source-a-'+source_ids[i]+'" href="'+source_links[i]+'" target="_blank">');
				$("#source-link-"+source_ids[i]).wrap('<a class="source-a-'+source_ids[i]+'" href="'+source_links[i]+'" target="_blank">');
			}

			$("#inputProbSource").append('<option value='+source_ids[i]+'>'+source_names[i]+'</option>');
			$("#editProbSource").append('<option value='+source_ids[i]+'>'+source_names[i]+'</option>');
		}
	}
}