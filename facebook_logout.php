<?php

include 'php/facebook.php';

session_start();
$fb_access_token = $_SESSION['fb_access_token'];

if ($fb_access_token)
{
	$fb_graph_url = "https://graph.facebook.com/me/permissions?method=delete&access_token="
		.$fb_access_token;

	$result = json_decode(file_get_contents($fb_graph_url));
	if ($result)
	{
		session_destroy();
		$status = 0;
	}
}
else
	$status = 1;
?>

<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Problems Solved - Log Out from Facebook</title>

		<!--Load CSS.-->
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/bootstrap-responsive.min.css">
		<link rel="stylesheet" href="css/login.css">

		<!--Load JS. For those that have the option to use CDN, please still include a copy for development use and include the code to import them while have them commented.-->
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<!--<script src="js/jquery-1.9.1.min.js"></script>-->
		<script src="js/bootstrap.min.js"></script>
		<script>
			setTimeout(function(){
				window.location.href = "login.php";
			}, 5000);
		</script>
	</head>
	<body>
		<div class="row-fluid">
			<div class="span6 offset3 login-content">
				<h3>
					<?php if ($status):?>
						You are not logged in!
					<?php else:?>
						You are now logged out from Facebook.
					<?php endif;?>
					<br>
					<small>
						You will be redirected in a short while.
					</small>
				</h3>
			</div>
		</div>
	</body>
</html>