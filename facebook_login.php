<?php
include "mysql/mysql.php";
include "php/facebook.php";

session_start();

if (!isset($_REQUEST['code'], $_REQUEST['state']))
{
	header("Location: login.php");
	die();
}

if (!array_key_exists('fb_state', $_SESSION))
{
	header("Location: login.php");
	die();
}

$fb_code = $_REQUEST['code'];
$fb_state = $_REQUEST['state'];

if ($_SESSION['fb_state'] === $fb_state)
{
	$fb_token_url = "https://graph.facebook.com/oauth/access_token?"
		."client_id=".FB_APP_ID."&redirect_uri=".urlencode(FB_REDIRECT)
		."&client_secret=".FB_APP_SECRET."&code=".$fb_code;

	$response = file_get_contents($fb_token_url);
	$params = null;
	parse_str($response, $params);
	if (!array_key_exists('access_token', $params))
	{
		header("Location: login.php");
		die();
	}
	$_SESSION['fb_access_token'] = $params['access_token'];
	$access_token = $_SESSION['fb_access_token'];

	$user = GetFbUserArrayFromFbAccessToken($access_token);
	if (!array_key_exists("id", $user))
	{
		header("Location: login.php");
		die();
	}

	$fb_id = $user['id'];
	$uid = GetUIDFromFbID($fb_id, $mysql_db);

	if ($uid == -1)
	{
		$mysql_db->query("INSERT INTO ".MYSQL_PREFIX."users (facebook_id) VALUES ('$fb_id')");
		$uid = $mysql_db->insert_id;
	}

	header("Location: index.php");
}
else
	echo "State not match";
?>